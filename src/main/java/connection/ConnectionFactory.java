package connection;


import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ConnectionFactory {
	
    private static final Logger LOGGER = Logger.getLogger(ConnectionFactory.class.getName());
    private static final String DRIVER = "com.mysql.cj.jdbc.Driver";
    private static final String DBURL = "jdbc:mysql://localhost:3306/Warehouse";
    private static final String USER = "root";
    private static final String PASS = "root";

    private static ConnectionFactory singleInstance = new ConnectionFactory();

    /***
     * method registers JDBC driver
     */
    private ConnectionFactory() {
        try 
        {
            Class.forName(DRIVER);
        }
        catch (ClassNotFoundException e) 
        {
            e.printStackTrace();
        }
    }

    /***
     * method creates connection
     * @return connection
     */
    private Connection createConnection() {
        Connection connection = null;
        try 
        {
            connection = DriverManager.getConnection(DBURL, USER, PASS);
            if (connection != null) return connection;
            else System.out.println("Failed to make connection!");
        } 
        catch (SQLException e) 
        {
            System.err.format("SQL State: %s\n%s", e.getSQLState(), e.getMessage());
        } 
        catch (Exception e) 
        {
            e.printStackTrace();
        }
        
        return connection;
    }

    /***
     * method returns connection
     * @return singleInstance.createConnection()
     */
    public static Connection getConnection() {
        return singleInstance.createConnection();
    }

    /***
     * method closes connection
     * @param connection
     */
    public static void close(Connection connection) {
        if (connection != null) {
            try 
            {
                connection.close();
            } 
            catch (SQLException e) 
            {
                LOGGER.log(Level.WARNING, "An error occured while trying to close the connection");
            }
        }
    }

    /***
     * method closes statement
     * @param preparedStatement
     */
    public static void close(PreparedStatement preparedStatement) {
        if (preparedStatement != null) 
        {   try 
            {
                preparedStatement.close();
            }
            catch (SQLException e) 
            {
                LOGGER.log(Level.WARNING, "An error occured while trying to close the statement");
            }
        }
    }

    /***
     * method closes ResultSet
     * @param resultSet
     */
    public static void close(ResultSet resultSet) {
        if (resultSet != null) 
        {
            try 
            {
                resultSet.close();
            } 
            catch (SQLException e)
            {
                LOGGER.log(Level.WARNING, "An error occured while trying to close the ResultSet");
            }
        }
    }


}



