drop database if exists Warehouse;
create database if not exists Warehouse;
use Warehouse;

drop table if exists Customer;
create table if not exists Customer(
clientName varchar(30) primary key ,
clientAddress varchar(30)
);

drop table if exists Product;
create table if not exists Product(
productName varchar(30) ,
productQuantity int primary key, 
productPrice double
);

drop table if exists Orders;
create table if not exists Orders(
orderClient varchar(30) ,
orderProduct varchar(30),
orderQuantity int
#foreign key (clientName) references Client (name),
#foreign key (productQuantity) references Product (quantity)
);

SET GLOBAL time_zone = '+2:00';






