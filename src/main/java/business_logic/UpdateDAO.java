package business_logic;

import java.util.logging.Logger;
import java.util.logging.Level;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.lang.reflect.Field;

import connection.ConnectionFactory;


public class UpdateDAO<T> {
	
	private static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName() );
    private Class<T> type = null;


    /***
     * UpdateDAO construct
     * @param clasa
     */
    public UpdateDAO(Class<T> clasa) {
        this.type = clasa;
    }
	
    /***
     * method updates Object
     * @param t
     */
	 public void update(T t) {
	    	 
		 	StringBuilder sb = new StringBuilder();
	        Connection connection = null;
	        PreparedStatement statement = null;
	       
	        try {
	        	
	        	Field[] field = t.getClass().getDeclaredFields();
	            int n=field.length - 1;
	            sb.append("update ");
	            sb.append(type.getSimpleName());
	            sb.append(" set ");
	            
	            for (int i = 0; i < n; i++) 
	            {
	                field[i].setAccessible(true);
	                Object object = field[i].get(t);
	                sb.append(field[i].getName() + " = '" + object + "', ");
	            }
	            field[n].setAccessible(true);
	            Object object2 = field[n].get(t);
	            sb.append(field[n].getName() + " = '" + object2 + "' where (");
	            Object object3 = field[0].get(t);
	            sb.append(field[0].getName() + " = '" + object3 + "')");
	        }
	        catch (IllegalAccessException e) {
	            e.printStackTrace();
	        }

	        try {
	            connection = ConnectionFactory.getConnection();
	            statement = connection.prepareStatement( sb.toString() );
	            statement.execute();
	        }
	        catch (SQLException e) 
	        {
	            e.printStackTrace();
	            LOGGER.log(Level.WARNING, type.getName() + "UpdateDAO " + e.getMessage());
	        }
	        ConnectionFactory.close(statement);
	        ConnectionFactory.close(connection);
	    }

}
