package presentation;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import org.apache.pdfbox.pdmodel.font.PDType1Font;

import data_access.CustomerDAO;
import data_access.OrdersDAO;
import model.Customer;
import model.Product;
import model.Orders;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.stream.Stream;


public class OrderReport {
    
	private static int orderID = 0;  
    private static int receiptID = 0;      

    List<model.Orders> orderss;

    /***
     * method generates OrderReport PDF
     */
    public void createPDF() {
        if ( (orderss = OrdersDAO.findOrders() ) == null) {
            System.out.println("No orders have been found!");
            return;
        }
        Document doc = new Document();
        orderID++;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("Order" + orderID + ".pdf");
            PdfWriter.getInstance(doc, new FileOutputStream( sb.toString() ) );
        }  
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }
        doc.open();
        PdfPTable pdftable = new PdfPTable(3);
        Stream.of("orderClient", "orderProduct", "orderQuantity").forEach(columnTitle -> {
                    PdfPCell topCell = new PdfPCell();
                    topCell.setBackgroundColor(BaseColor.GREEN);
                    topCell.setBorderWidth(3);
                    topCell.setPhrase(new Phrase(columnTitle));
                    pdftable.addCell(topCell);
                } );

        for (Orders o : orderss) {
            pdftable.addCell(o.getOrderClient() );
            pdftable.addCell(o.getOrderProduct() );
            pdftable.addCell(o.getOrderQuantity() + "");
        }
        try {
            doc.add(pdftable);
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }
        doc.close(); 
    }

    /***
     * method generates Receipt PDF for order
     * @param order
     * @param product
     */
    public void receiptPDF(Orders order, Product product) {
        receiptID++;
        Customer client = CustomerDAO.findNameOfCustomer(order.getOrderClient());
        PDDocument doc = new PDDocument();
        PDPage pg = new PDPage();
        doc.addPage(pg);
        try {
            PDPageContentStream showContent = new PDPageContentStream(doc, pg);
            showContent.beginText();
            showContent.setFont(PDType1Font.COURIER, 12);
            showContent.newLineAtOffset(30, 800);
            showContent.setLeading(15.5);
            String bill = "*RECEIPT*";
            showContent.showText(bill);
            showContent.newLine();
            String nameC = "Name of Client: " + client.getClientName();
            showContent.showText(nameC);
            showContent.newLine();
            String addrC = "Address of Client: " + client.getClientAddress();
            showContent.showText(addrC);
            showContent.newLine();
            String nameP = "Name of Product: " + product.getProductName();
            showContent.showText(nameP);
            showContent.newLine();
            String priceP = "Product Price " + product.getProductPrice();
            showContent.showText(priceP);
            showContent.newLine();
            String stockP = "Product Quantity: " + order.getOrderQuantity();
            showContent.showText(stockP);
            showContent.newLine();
            String total = "Total Price = " + order.getOrderQuantity() * product.getProductPrice();
            showContent.showText(total);
            showContent.newLine();
            showContent.endText();
            showContent.close();
            
            StringBuilder sb = new StringBuilder();
            sb.append("Receipt" + receiptID + ".pdf");
            doc.save( sb.toString() );
            doc.close();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

    }

}
