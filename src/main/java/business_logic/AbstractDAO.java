package business_logic;

import java.util.logging.Logger;
import java.sql.SQLException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import connection.ConnectionFactory;


public class AbstractDAO<T> {
    private static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName() );
    private Class<T> type = null;

    /***
     * AbstractDAO construct
     * @param clasa
     */
    public AbstractDAO(Class<T> clasa) {
        this.type = clasa;
    }

    /***
     * method for select from database
     * @param field
     * @return
     */
    private String createSelectQuery(String field) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(" * ");
        sb.append(" FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE " + field + " =?");
        return sb.toString();
    }


    /***
     * method creates Objects
     * @param resultSet
     * @return
     */   
    private List<T> createObjects(ResultSet resultSet) {
        List<T> list = new ArrayList<T>();

        try {
            while (resultSet.next()) {
                //@SuppressWarnings("deprecation")
				T instance = type.newInstance();
                for (Field field : type.getDeclaredFields()) {
                    Object value = resultSet.getObject(field.getName());
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                    Method method = propertyDescriptor.getWriteMethod();
                    method.invoke(instance, value);
                }
                list.add(instance);
            }
        }
        catch (InstantiationException e) {
            e.printStackTrace();
        }  
        catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        catch (IntrospectionException e) {
            e.printStackTrace();
        }
        catch (IllegalAccessException e) {
            e.printStackTrace();
        }  
        catch (InvocationTargetException e) {
            e.printStackTrace();
        }
        catch (SQLException e) {
            e.printStackTrace();
        }
        catch (SecurityException e) {
            e.printStackTrace();
        }
      
        return list;
    }
    
    /***
     * method shows all info
     * @return
     */
    public List<T> findAll() {
    	  
    	StringBuilder sb = new StringBuilder();
        sb.append("select * from ");
        sb.append(type.getSimpleName());
        
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
      
        String query = sb.toString();
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();
            List<T> list = createObjects(resultSet);
            if (list.isEmpty() == true)
                return null;
            else
                return list;
        }
        catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "FindAllDAO" + e.getMessage());
        }
        ConnectionFactory.close(resultSet);
        ConnectionFactory.close(statement);
        ConnectionFactory.close(connection);
        return null;
    }
    
    /***
     * method for searching Object by ID
     * @param id
     * @return
     */
    public T findById(int id) {
        
    	Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectQuery("id");
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, id);
            resultSet = statement.executeQuery();
            return createObjects(resultSet).get(0);
        }
        catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "FindByIdDAO " + e.getMessage());
        }
        finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }


    /***
     * method finds Object by name
     * @param field
     * @param name
     * @return
     */
    public T findByName(String field, Object name) {
        
    	StringBuilder sb = new StringBuilder();
        sb.append("select * from ");
        sb.append(type.getSimpleName());
        sb.append(" where " + field + " = ?");
        
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        
        String query = sb.toString();

        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setString(1, (String) name);
            resultSet = statement.executeQuery();
            List<T> list = createObjects(resultSet);
            if (list.isEmpty() == true) return null;
            else return list.get(0);
        }
        catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "FindByNameDAO " + e.getMessage());
        }
        ConnectionFactory.close(resultSet);
        ConnectionFactory.close(statement);
        ConnectionFactory.close(connection);
        return null;
    }
   

}
