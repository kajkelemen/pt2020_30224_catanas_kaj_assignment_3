package data_access;


import java.util.List;

import business_logic.AbstractDAO;
import business_logic.DeleteDAO;
import business_logic.InsertDAO;
import business_logic.UpdateDAO;
import model.Customer;


public class CustomerDAO extends AbstractDAO<Customer> {


	/***
     * CustomerDAO construct
     * @param clasa 
     */
    public CustomerDAO(Class<Customer> clasa) {
        super(clasa);
    }
   
    /***
     * method finds Customer by name
     * @param nameOfCustomer
     * @return
     */

    public static Customer findNameOfCustomer(String nameOfCustomer) {
        return new AbstractDAO<Customer>(Customer.class).findByName("clientName", nameOfCustomer);
    }

    /***
     * method finds Customers
     * @return
     */
    public static List<Customer> findCustomers() {
        return new AbstractDAO<Customer>(Customer.class).findAll();
    }

    /***
     * method inserts Customer
     * @param clientToInsert
     */
    public static void insertCustomer(Customer clientToInsert) {
        new InsertDAO<Customer>(Customer.class).insert(clientToInsert);
    }

    /***
     * method deletes Customer
     * @param clientToDelete
     */
    public static void deleteCustomer(String clientToDelete) {
        new DeleteDAO<Customer>(Customer.class).delete("clientName", clientToDelete);
    }
    
    /***
     * method updates Customer
     * @param clientName
     * @param clientAddress
     * @return newClient
     */
    public Customer updateCustomer(String clientName, String clientAddress) {
        Customer oldClient = findNameOfCustomer(clientName);
        Customer newClient = new Customer(clientName, clientAddress);
        new UpdateDAO<Customer>(Customer.class).update(oldClient);
        return newClient;
    }

}
