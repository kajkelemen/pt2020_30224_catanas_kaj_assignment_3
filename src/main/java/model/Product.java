package model;


public class Product {
    private String productName;
    private int productQuantity;
    private double productPrice;

 
    /***
     * Product construct
     * @param productName
     * @param productQuantity
     * @param productPrice
     */
    
    public Product(String productName, int productQuantity, double productPrice) {
        this.productName = productName;
        this.productQuantity = productQuantity;
        this.productPrice = productPrice;
    } 
    
    public Product() {
        super();
    }
    
    public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getProductQuantity() {
		return productQuantity;
	}

	public void setProductQuantity(int productQuantity) {
		this.productQuantity = productQuantity;
	}

	public double getProductPrice() {
		return productPrice;
	}

	public void setProductPrice(double productPrice) {
		this.productPrice = productPrice;
	}

	/***
     * method shows Product info
     * @return
     */
	public String toString() {
        return this.productName + " " + this.productQuantity + " " + this.productPrice;
    }



}
