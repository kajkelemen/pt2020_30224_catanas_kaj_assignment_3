package business_logic;

import connection.ConnectionFactory;
import java.util.logging.Logger;
import java.util.logging.Level;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;
import java.lang.reflect.Field;


public class InsertDAO<T> {

    private static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName() );
    private Class<T> type = null;

    
    /***
     * InsertDAO construct
     * @param clasa
     */
    public InsertDAO(Class<T> clasa) {
        this.type = clasa;
    }


    /***
     * method inserts Object
     * @param t
     */
    public void insert(T t) {
       
    	StringBuilder sb = new StringBuilder();
        sb.append("insert into ");
        sb.append(type.getSimpleName());
        sb.append(" (");
        
        Connection connection = null;
        PreparedStatement statement = null;
        int n = 0;
        for (Field field : type.getDeclaredFields()) {
            
        	if (n != 0) sb.append(",");
            sb.append(field.getName() );
            n++;
        }
        sb.append(") values (");
        for (int i = 0; i < n; i++) {
           
        	if (i != 0) sb.append(",");
            sb.append("?");
        }
        sb.append(")");
        try {
        	Field[] newfield = t.getClass().getDeclaredFields();
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(sb.toString());
            
            for (int i = 0; i < newfield.length; i++) {
            	newfield[i].setAccessible(true);
                Object object = newfield[i].get(t);
                statement.setObject(i+1, object);
            }
            statement.execute();
        }
        catch (SQLException e) {
            e.printStackTrace();
            LOGGER.log(Level.WARNING, type.getName() + "InsertDAO " + e.getMessage());
        }
        catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        ConnectionFactory.close(statement);
        ConnectionFactory.close(connection);
    }


}
