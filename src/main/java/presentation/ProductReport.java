package presentation;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.BaseColor;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.List;
import java.util.stream.Stream;

import data_access.ProductDAO;
import model.Product;



public class ProductReport {

    private static int reportID = 0;
    List<Product> products;
    
    /***
     * method generates ProductReport PDF
     */
    public void createPDF() {
        if ( (products = ProductDAO.findProducts() ) == null) {
            System.out.println("Product not found!");
            return;
        }
        reportID++;
        Document doc = new Document();
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("ReportProduct" + reportID + ".pdf");
            PdfWriter.getInstance(doc, new FileOutputStream( sb.toString() ) );
        }
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }
        
        doc.open();
        PdfPTable pdftable = new PdfPTable(3);
        TopOfTable(pdftable);
        addRow(pdftable);
        try {
            doc.add(pdftable);
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }
        doc.close();
    }

    /***
     * method for top cell in table
     * @param table
     */
    private void TopOfTable(PdfPTable table) {
        Stream.of("productName", "productQuantity", "productPrice").forEach(columnTitle -> {
                    PdfPCell topCell = new PdfPCell();
                    topCell.setBackgroundColor(BaseColor.GREEN);
                    topCell.setBorderWidth(3);
                    topCell.setPhrase(new Phrase(columnTitle));
                    table.addCell(topCell);
        } );
    }


    /***
     * method adds row in table
     * @param table
     */
    private void addRow(PdfPTable table) {
        for (Product p : products) {
            table.addCell(p.getProductName() );
            table.addCell(p.getProductQuantity() + "");
            table.addCell(p.getProductPrice() + "");
        }
    }

}
