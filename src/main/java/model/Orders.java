package model;

public class Orders {
	
    private String orderClient;
    private String orderProduct;
    private int orderQuantity;

    /***
     * Order construct
     * @param orderClient
     * @param orderProduct
     * @param orderQuantity
     */
    
    public Orders(String orderClient, String orderProduct, int orderQuantity) {
        this.orderClient = orderClient;
        this.orderProduct = orderProduct;
        this.orderQuantity = orderQuantity;
    } 
    
    public Orders() {
        super();
    }

    public String getOrderClient() {
		return orderClient;
	}


	public void setOrderClient(String orderClient) {
		this.orderClient = orderClient;
	}


	public String getOrderProduct() {
		return orderProduct;
	}


	public void setOrderProduct(String orderProduct) {
		this.orderProduct = orderProduct;
	}


    public int getOrderQuantity() {
		return orderQuantity;
	}


	public void setOrderQuantity(int orderQuantity) {
		this.orderQuantity = orderQuantity;
	}

	/***
     * method shows Order info
     * @return
     */
	
	public String toString() {
        return this.orderClient + " " + this.orderProduct + " " + this.orderQuantity;
    }



}
