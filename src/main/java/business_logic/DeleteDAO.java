package business_logic;

import java.util.logging.Logger;
import java.util.logging.Level;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.PreparedStatement;


import connection.ConnectionFactory;

public class DeleteDAO<T> {
	
	private static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName() );
    private Class<T> type = null;

    /***
     * DeleteDAO construct
     * @param clasa
     */
    public DeleteDAO(Class<T> clasa) {
        this.type = clasa;
    }
	
    /***
     * method deletes Object
     * @param field
     * @param name
     */
	public void delete(String field, String name) {
        
		StringBuilder sb = new StringBuilder();
        sb.append("delete from ");
        sb.append(type.getSimpleName());
        sb.append(" where " + field + " = ?");
		
        Connection connection = null;
        PreparedStatement statement = null;
        
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(sb.toString());
            statement.setString(1, name);
            statement.execute();
        }
        catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DeleteDAO " + e.getMessage());
        }
        ConnectionFactory.close(statement);
        ConnectionFactory.close(connection);
    }

}
