package presentation;


import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import java.util.List;
import java.util.stream.Stream;
import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;

import data_access.CustomerDAO;
import model.Customer;

public class CustomerReport {
    
    private List<Customer> customers;
    private static int reportID = 0;
    
    
    /***
     * method generates CustomerReport PDF
     */
    public void create_PDF() {
        
    	if ( (customers = CustomerDAO.findCustomers() ) == null) {
            System.out.println(" Customer has not been found! ");
        }
        Document doc= new Document();
        reportID++;
        try {
            StringBuilder sb = new StringBuilder();
            sb.append("ReportClient" + reportID + ".pdf");
            PdfWriter.getInstance(doc, new FileOutputStream(sb.toString()));
        } 
        catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }
        doc.open();
        PdfPTable pdftab = new PdfPTable(2);
        TopOfTable(pdftab);addRow(pdftab);
        
        try {
            doc.add(pdftab);
        }
        catch (DocumentException e) {
            e.printStackTrace();
        }
        doc.close(); 
    }

    /***
     * method for top cell in table
     * @param table
     */
    private void TopOfTable(PdfPTable table) {
        Stream.of("clientName", "clientAddress").forEach(col -> {
            PdfPCell topCell = new PdfPCell();
            topCell.setBackgroundColor(BaseColor.GREEN);
            topCell.setBorderWidth(3);
            topCell.setPhrase(new Phrase(col)) ;
            table.addCell(topCell);
        } );
    }
    
    /***
     * method adds row in table
     * @param table
     */
    private void addRow(PdfPTable table) {
        for (Customer c : customers) {
            table.addCell(c.getClientName());
            table.addCell(c.getClientAddress());
        }
    }

}
