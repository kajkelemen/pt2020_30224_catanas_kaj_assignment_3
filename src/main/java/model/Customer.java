package model;

public class Customer {
    private String clientName;
    private String clientAddress;


    /***
     * Customer construct
     * @param clientName
     * @param clientAddress
     */
    
    public Customer(String clientName, String clientAddress) {
        this.clientName = clientName;
        this.clientAddress = clientAddress;
    }  
    
    public Customer() {
        super();
    }
 
    public String getClientName() {
		return clientName;
	}

	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	
	public String getClientAddress() {
		return clientAddress;
	}

	public void setClientAddress(String clientAddress) {
		this.clientAddress = clientAddress;
	}

	/***
     * method shows Client info
     * @return
     */
	public String toString() {
        return this.clientName + " " + this.clientAddress;
    }

}
