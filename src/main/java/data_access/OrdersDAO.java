package data_access;


import model.Orders;
import java.util.List;

import business_logic.AbstractDAO;
import business_logic.DeleteDAO;
import business_logic.InsertDAO;


public class OrdersDAO extends AbstractDAO<Orders> {

	/***
     * OrdersDAO construct
     * @param clasa
     */
    public OrdersDAO(Class<Orders> clasa) {
        super(clasa);
    }

    /***
     * method finds Order
     * @return
     */
    public static List<Orders> findOrders() {
        return new AbstractDAO<Orders>(Orders.class).findAll();
    }

    /***
     * method places Order
     * @param order
     */
    public static void insertOrder(Orders order) {
        new InsertDAO<Orders>(Orders.class).insert(order);
    }

    /***
     * method deletes Order
     * @param orders
     * @param order
     */
    public static void deleteOrder(String orders,String order) {
        new DeleteDAO<Orders>(Orders.class).delete(orders,order);
    }

}
