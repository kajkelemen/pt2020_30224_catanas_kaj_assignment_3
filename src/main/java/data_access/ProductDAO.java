package data_access;


import model.Product;
import java.util.List;

import business_logic.AbstractDAO;
import business_logic.DeleteDAO;
import business_logic.InsertDAO;
import business_logic.UpdateDAO;


public class ProductDAO extends AbstractDAO<Product>{

	/***
     * ProductDAO construct
     * @param clasa
     */
    public ProductDAO(Class<Product> clasa) {
        super(clasa);
    }
    /***
     * ProductDAO simple construct
     */
    public ProductDAO() {
        super(Product.class);
    }
  
    /***
     * method finds Product by name
     * @param productName
     * @return
     */
    public static Product findNameOfProduct(String productName) {
        return new AbstractDAO<Product>(Product.class).findByName("productName", productName);
    }
    
    /***
     * method finds Products
     * @return
     */
    public static List<Product> findProducts() {
        return new AbstractDAO<Product>(Product.class).findAll();
    }
  
    /***
     * method inserts Product
     * @param product
     */
    public static void insertProduct(Product product) {
        new InsertDAO<Product>(Product.class).insert(product);
    }

    /***
     * method deletes Product
     * @param productName
     */
    public static void deleteProduct(String productName) {
        new DeleteDAO<Product>(Product.class).delete("productName", productName);
    }

    /***
     * method updates Product(subtracts quantity)
     * @param productName
     * @param productQuantity
     * @return newProduct
     */
    public Product updateProduct(String productName, int productQuantity) {
        Product oldProduct = findNameOfProduct(productName);
        Product newProduct = new Product(productName, productQuantity, oldProduct.getProductPrice() );
        oldProduct.setProductQuantity(oldProduct.getProductQuantity() - productQuantity);
        new UpdateDAO<Product>(Product.class).update(oldProduct);
        return newProduct;
    }
    /***
     * method updates Product(adds quantity)
     * @param productName
     * @param productQuantity
     */
    public static void addQuantity(String productName, int productQuantity) {
        Product oldProduct = findNameOfProduct(productName);
        oldProduct.setProductQuantity(oldProduct.getProductQuantity() + productQuantity);
        new UpdateDAO<Product>(Product.class).update(oldProduct);
    }


}
