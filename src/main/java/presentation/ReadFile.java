package presentation;

import java.io.File;
import java.util.Scanner;
import java.io.FileNotFoundException;


import data_access.CustomerDAO;
import data_access.OrdersDAO;
import data_access.ProductDAO;
import model.Customer;
import model.Product;
import model.Orders;


public class ReadFile {

	/***
     * method read from FILE
     * @param fileToRead
     */
    public static void readInputFile(String fileToRead) {
        
    	File readFile = new File(fileToRead);
        Scanner in = null;
        
        try {
            	in = new Scanner(readFile);
            	while (in.hasNext() ) 
            	{
            		String str = in.nextLine();
            		String[] command = str.split(": ");

            		switch (command[0])
            		{
               
            		case "Insert client": {
            			String[] c = command[1].split(", ");
                    if (CustomerDAO.findNameOfCustomer(c[0]) == null)
                        CustomerDAO.insertCustomer(new Customer(c[0], c[1]));
                    break;
            		}
            		
            		case "Delete client":{
                    String[] c = command[1].split(", ");
                    if (CustomerDAO.findNameOfCustomer(c[0]) != null) CustomerDAO.deleteCustomer(c[0]);
                    break;
          			}
            		
            		case "Report client":{
                    CustomerReport cr = new CustomerReport();
                    if (CustomerDAO.findCustomers() != null) cr.create_PDF();
                    break;
            		}
            		
            		case "Insert product": {
                    String[] c = command[1].split(", ");
                    if (ProductDAO.findNameOfProduct(c[0]) == null)
                        ProductDAO.insertProduct(new Product(c[0], Integer.parseInt(c[1]), Double.parseDouble(c[2])));
                   // else { ProductDAO.addQuantity(c[0], Integer.parseInt(c[1]));}
                    	
                    break;
            		}
            		
            		case "Delete Product":{
                    String[] c = command[1].split(", ");
                    if (ProductDAO.findNameOfProduct(c[0]) != null) ProductDAO.deleteProduct(c[0]);
                    break;
          			}
            			
            		case "Report product":{
                    ProductReport pr = new ProductReport();
                    if (ProductDAO.findProducts() != null) pr.createPDF();
                    break;
            		}
            		
          			case "Order": { 
          			String[] c = command[1].split(", ");
                    Product product = ProductDAO.findNameOfProduct(c[1]);
                    if (product.getProductQuantity() < Integer.parseInt(c[2])) 
                    {
                        new OrderReport();
                    }
                    else 
                    {
                        Orders orders = new Orders(c[0], c[1], Integer.parseInt(c[2]));
                        OrdersDAO.insertOrder(orders);
                        ProductDAO productDAO = new ProductDAO();
                        Product orderedProduct = productDAO.updateProduct(c[1], Integer.parseInt(c[2]));
                        OrderReport or = new OrderReport();
                        or.receiptPDF(orders, orderedProduct);
                    }
                    break;
          			}

            		case "Report order":{
                    OrderReport or = new OrderReport();
                    if (OrdersDAO.findOrders() != null) or.createPDF();
                    break;
            		}
              
            		default:{
                    System.out.println(command[0]+" Error!\n Invalid data from FILE!");break;
            		}
            		
            		}

            	}
        	}
        	catch (FileNotFoundException e) {
            e.printStackTrace();
        	}

            in.close();

    }

}
